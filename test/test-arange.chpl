use NumpyLike;

var F = openmem();
var Fw = F.writer();
test(Fw);
Fw.close();
var Fr = F.reader();
var result="".join(Fr.lines());
var expected = """ 0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9
 0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9
0.0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9
 0 0.588 0.951 0.951 0.588 1.22e-16 -0.588 -0.951 -0.951 -0.588
0.0 1.0 2.0 3.0 4.0 5.0 6.0 7.0 8.0 9.0
0.0 1.0 2.0 3.0 4.0 5.0 6.0 7.0 8.0 9.0
""";
if expected != result then exit(1);




proc test(chan) {
  // Example of simple iteration
  for xi in arange(0.0,0.99,0.1) do chan.writef(" %r",xi);
  chan.writef("\n");
  for xi in arange(0,0.99,0.1,dtype=real) do chan.writef(" %r",xi);
  chan.writef("\n");


  // But Chapel allows other wonderful things once you
  // define an iterator

  // Array assignment
  var x = arange(0, 0.999, 0.1, dtype=real);
  chan.writeln(x);

  // Promote a scalar function
  var y = sin(2*pi*arange(0, 0.999, 0.1, dtype=real));
  for yi in y do chan.writef(" %.3r",yi);
  chan.writef("\n");

  // Step is optional
  chan.writeln(arange(0, 9.9, dtype=real));
  chan.writeln(arange(0.0, 10.0));
}
