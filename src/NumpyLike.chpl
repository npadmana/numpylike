/* This module provides some Numpy look-alike
   functions. As far as reasonable, these are pretty
   close in calling convention to the equivalent numpy
   functions.

*/

module NumpyLike {

  /* Return evenly spaced elements between [start, stop).

     Specifying the type allows one to coerce the output
     to a particular type.

     This is the most generic form, with all parameters specified.

     If step is a floating point number, then stop may/may not
     be included, depending on how the rounding is done. 

     Note that, unlike in Numpy, the type is specified at the
     front.
  */
  iter arange(type dtype, start, stop, step=1:dtype) {
    assert(stop > start, "Stop must be greater than start");
    assert(step > 0, "Step must be greater than 0");
    var x : dtype = start;
    while (x < stop) {
      yield x;
      x += step;
    }
  }

  /* In this case, the type is inferred from start.

     step defaults to 1 here.
  */
  iter arange(start:?t, stop:t, step:t=1:t) {
    for x in arange(t, start, stop, step) do yield x;
  }

  /* Defaults to start=0, step=1, and a specified type */
  iter arange(type dtype, stop) {
    for x in arange(dtype, 0, stop, 1) do yield x;
  }

  /* start=0, step=1, and type inferred from stop */
  iter arange(stop:?t) {
    for x in arange(t, 0, stop, 1) do yield x;
  }

  /*
    Generate num linearly spaced elements between
    start and stop of type dtype.

    If endpoint is true, then the final value is also included.

    Currently, start and stop only take in scalar values like the
    older Numpy functions.

    In general, linspace returns real values, unless coerced.
   */
  iter linspace(type dtype, start, stop, num, in endpoint:bool=true) {
    assert(num > 0, "number of points must be > 0");
    if num==1 then endpoint = false;
    const ninterval = (if endpoint then num-1 else num):real;
    const dt = (stop-start)/ninterval;

    for i in 0..#num do yield (start+i*dt):dtype;
  }


  iter linspace(start, stop, num, endpoint=true) {
    for x in linspace(real(64), start, stop, num, endpoint) do yield x;
  }


  /*
    Generate num logspaced spaced elements. 

    The starting value is base**start, while the end is
    base**stop.

    If endpoint is true, then the final value is also included.

    Currently, start and stop only take in scalar values like the
    older Numpy functions.

    In general, logspace returns real values, unless coerced.
   */
  iter logspace(type dtype, start, stop, num,endpoint:bool=true, base=10.0) {
    for x in linspace(real(64), start, stop, num, endpoint) do
      yield (base**x):dtype;
  }

  iter logspace(start, stop, num,endpoint:bool=true, base=10.0) {
    for x in logspace(real(64), start, stop, num, endpoint, base) do
      yield x;
  }

  /*
    Generate num logspaced spaced elements. 

    The starting value is start, while the end is stop.

    If endpoint is true, then the final value is also included.

    Currently, start and stop only take in scalar values like the
    older Numpy functions.

    In general, logspace returns real values, unless coerced.
   */
  iter geomspace(type dtype, start, stop, num,endpoint:bool=true) {
    const sgn1 = sgn(start);
    const sgn2 = sgn(stop);
    assert(sgn1==sgn2,"start and stop must have the same sign");
    assert(sgn1!=0,"start/stop cannot be zero");
    for x in linspace(real(64), log(sgn1*start:real),
                      log(sgn2*stop:real), num, endpoint) do
      yield sgn1*exp(x);
  }

  iter geomspace(start, stop, num,endpoint:bool=true, base=10.0) {
    for x in geomspace(real(64), start, stop, num, endpoint) do
      yield x;
  }

}